package dam.monitordam;

/**
 * Created by uriel on 10/10/17.
 */

public class Doctor {
    String nombre, direccion, telefono, email, user, pass;
    public Doctor(String nombre, String direccion, String telefono, String email, String user, String pass){
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.user = user;
        this.pass = pass;
    }
}
