package dam.monitordam;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import java.util.Date;

public class PatientsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients);
    }
    public void cancelPatient(View view) {
        Intent intent = new Intent(this, start.class);
        startActivity(intent);
    }

    public void registerPatient(View view) {
        String nombre=((EditText)findViewById(R.id.nameP)).getText().toString();
        String direccion=((EditText)findViewById(R.id.dirP)).getText().toString();
        String telefono=((EditText)findViewById(R.id.telP)).getText().toString();
        String edad=((EditText)findViewById(R.id.ageP)).getText().toString();
        String peso=((EditText)findViewById(R.id.weightP)).getText().toString();
        String altura=((EditText)findViewById(R.id.heightP)).getText().toString();
        String fechadenacimiento=((EditText)findViewById(R.id.borndateP)).getText().toString();
        String medicamento=((EditText)findViewById(R.id.medicineP)).getText().toString();
        String email=((EditText)findViewById(R.id.emailP)).getText().toString();
        String usuario=((EditText)findViewById(R.id.userP)).getText().toString();
        String pass=((EditText)findViewById(R.id.passP)).getText().toString();

        AlertDialog.Builder wrong = new  AlertDialog.Builder(this);
        wrong.setMessage("Algun campo esta vacio")
                .setTitle("AVISO").setCancelable(false)
                .setNeutralButton("Aceptar",
                        new  DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                dialog.cancel();
                            }
                        });
        AlertDialog.Builder welcome = new  AlertDialog.Builder(this);
        welcome.setMessage("Usuario "+usuario+" registrado con exito")
                .setTitle("Bienvenido").setCancelable(false)
                .setNeutralButton("Aceptar",
                        new  DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                dialog.cancel();
                            }
                        });
        if (usuario.equals("") || pass.equals("") || email.equals("") || nombre.equals("") ) {
            AlertDialog alert = wrong.create();
            alert.show();
        }else{
            Paciente p = new Paciente(nombre, direccion, edad, peso,altura, telefono,email,usuario,pass,fechadenacimiento);
            AlertDialog alert = welcome.create();
            alert.show();
        }
    }

}
