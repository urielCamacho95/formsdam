package dam.monitordam;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class doctorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor);
    }
    public void cancelDoctor(View view) {
        Intent intent = new Intent(this, start.class);
        startActivity(intent);
    }

    public void registerDoctor(View view) {
        String nombre=((EditText)findViewById(R.id.nameD)).getText().toString();
        String direccion=((EditText)findViewById(R.id.dirD)).getText().toString();
        String telefono=((EditText)findViewById(R.id.telD)).getText().toString();
        String email=((EditText)findViewById(R.id.emailD)).getText().toString();
        String usuario=((EditText)findViewById(R.id.userD)).getText().toString();
        String pass=((EditText)findViewById(R.id.passD)).getText().toString();

        AlertDialog.Builder wrong = new  AlertDialog.Builder(this);
        wrong.setMessage("Algun campo esta vacio")
                .setTitle("AVISO").setCancelable(false)
                .setNeutralButton("Aceptar",
                        new  DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                dialog.cancel();
                            }
                        });
        AlertDialog.Builder welcome = new  AlertDialog.Builder(this);
        welcome.setMessage("Usuario "+usuario+" registrado con exito")
                .setTitle("Bienvenido").setCancelable(false)
                .setNeutralButton("Aceptar",
                        new  DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                dialog.cancel();
                            }
                        });
        if (usuario.equals("") || pass.equals("") || email.equals("") || nombre.equals("") ) {
            AlertDialog alert = wrong.create();
            alert.show();
        }else{
            Doctor d = new Doctor(nombre, direccion, telefono,email,usuario,pass);
            AlertDialog alert = welcome.create();
            alert.show();
        }
    }
}
