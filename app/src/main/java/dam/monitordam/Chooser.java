package dam.monitordam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Chooser extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chooser);
    }
    public void patient(View view) {
        Intent intent = new Intent(this, PatientsActivity.class);
        startActivity(intent);
    }
    public void doctor(View view) {
        Intent intent = new Intent(this, doctorActivity.class);
        startActivity(intent);
    }
}
